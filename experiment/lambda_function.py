import json
import locale
import time
import base64
from vendor import pdfkit
from vendor.pyqrcode import pyqrcode

import boto3
from botocore.exceptions import ClientError


def delete_objects(bucket_name, object_names):
    """Delete multiple objects from an Amazon S3 bucket

    :param bucket_name: string
    :param object_names: list of strings
    :return: True if the referenced objects were deleted, otherwise False
    """

    # Convert list of object names to appropriate data format
    objlist = [{'Key': obj} for obj in object_names]

    # Delete the objects
    s3 = boto3.client('s3')
    try:
        result = s3.delete_objects(Bucket=bucket_name, Delete={'Objects': objlist})
        print(result)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def lambda_handler(event, context):
    # TODO implement
    """Exercise delete_objects()"""

    # Assign these values before running the program
    test_bucket_name = 'furnibase-develop'
    # test_object_names = ['test_asset/1.JPG', 'OBJECT_NAME_02', 'test_asset/10.JPG']

    # delete_objects(test_bucket_name, test_object_names)
    # return {
    #     'statusCode': 200,
    #     'body': json.dumps('Done delete files')
    # }
    # write_to_s3(test_bucket_name)
    # generate_qr_png(test_bucket_name, 'https://develop.furnibase.com'
    html_to_pdf(test_bucket_name)


def generate_qr_png(bucket, url):
    qrcode = pyqrcode.create(url, version=5)
    qr_as_string = qrcode.png_as_base64_str(scale=12)
    decoded_file = base64.b64decode(qr_as_string)
    print(qr_as_string)
    print(decoded_file)
    filename = 'test_asset/test_qr.png'
    s3 = boto3.client('s3')
    s3.put_object(Bucket=bucket, Key=filename, Body=decoded_file)


def write_to_s3(bucket):
    # bucket = "furnibase-develop"
    file = '<html><head></head><body>This is sample html file.<img src="https://develop-cdn-furnibase.imgix.net/assets/images/furnibase-logo-2019-2.png?w=200&lossless=1"><img src="https://furnibase-develop.s3-ap-southeast-1.amazonaws.com/test_asset/11.JPG"></body></html>'
    encoded_file = file.encode("utf-8")
    filename = 'test_asset/test.html'
    s3 = boto3.client('s3')
    s3.put_object(Bucket=bucket, Key=filename, Body=encoded_file)


def locale_test():
    print(locale.getlocale())
    time_en = time.strftime("%A, %d %B %Y at %H:%M", time.localtime(1580284615 + 7 * 3600))
    print(time_en)
    locale.setlocale(locale.LC_TIME, "id_ID")
    print(locale.getlocale())
    time_id = time.strftime("%A, %d %B %Y at %H:%M", time.localtime(1580284615 + 7 * 3600))
    print(time_id)

def html_to_pdf(bucket):
    # Set file path to save pdf on lambda first (temporary storage)
    key = 'test.pdf'
    filepath = '/tmp/{key}'.format(key=key)
    url = "https://furnibase-develop.s3-ap-southeast-1.amazonaws.com/o2o-store/ready-print/qr-test-2.html"

    # Create PDF
    config = pdfkit.configuration(wkhtmltopdf="binary/wkhtmltopdf")
    options = {
        'quiet': '',
        'page-size': 'A4',
        'encoding': "UTF-8",
        'margin-top': '0.75in',
        'margin-right': '0.75in',
        'margin-bottom': '0.75in',
        'margin-left': '0.75in'
    }
    pdfkit.from_url(configuration=config, url=url, output_path=filepath, options=options)

    # upload to s3
    s3 = boto3.client('s3')
    s3_key = 'test_asset/test.pdf'
    s3.put_object(Bucket=bucket, Key=s3_key, Body=open(filepath, 'rb'))
    # s3.put_object(Bucket=bucket, Key=key, Body=pdf)
